// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false
};

export const HOST_CREDENTIALS = {
    hostURL: 'https://final-thesis-api.azurewebsites.net'
    //hostURL: 'http://192.168.178.100:8080'
};

export const GOOGLE_API_KET = {
    apiKey: 'AIzaSyARZRYjTqOnYbqqUFnoGnTrSjcBym7Y7Y8'
};


export const STRIPE_PUBLIC_KEY = {
    stripePublicKey: 'pk_test_NqZQAnjCk8pvCo7WHjZ1q85e00bB5B1gr1'
};

export const BILLING_CURRENCY = {
    currency: 'MKD'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
