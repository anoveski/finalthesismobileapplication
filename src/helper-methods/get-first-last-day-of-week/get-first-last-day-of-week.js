var getFirstLastDayOfWeek = function (userDate) {

    let result = {};

    let curr = new Date(userDate); // get current date
    let first = curr.getDate() - curr.getDay() + 1; // First day is the day of the month - the day of the week
    let last = first + 6; // last day is the first day + 6

    result = {
        firstDay: new Date(curr.setDate(first)).toUTCString(),
        lastDay: new Date(curr.setDate(last)).toUTCString()
    };

    return result;
};

export {getFirstLastDayOfWeek};
