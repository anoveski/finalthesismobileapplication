import { Injectable } from '@angular/core';
import {GOOGLE_API_KET} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LocationService {
  constructor(private  httpClient: HttpClient) {
  }

  getCurrentPosition() {
    return new Promise(
        (resolve, reject) => {
          if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((position) => {
              resolve({
                latitude: position.coords.latitude,
                longitude: position.coords.longitude
              });
            });
          } else {
            console.log('not location');
            reject(false);
          }
        });
  }

  getLocationData() {
    return new Promise(
        (resolve, reject) => {
          this.getCurrentPosition()
              .then((position: any) => {
                this.googleLocationData({
                  latitude: position.latitude,
                  longitude: position.longitude,
                }).then(data => {
                  resolve(data);
                }).catch(error => {
                  reject(error);
                });
              }).catch(error => {
            reject(error);
          });
        }
    );
  }

  private googleLocationData(body) {
    return new Promise(
        (resolve, reject) => {
          this.httpClient.get('https://maps.googleapis.com/maps/api/geocode/json?latlng=' +
              body.latitude + ',' + body.longitude + '&result_type=administrative_area_level_1&sensor=true&key=' + GOOGLE_API_KET.apiKey)
              .subscribe((data: any) => {
                resolve(data);
              }, (error) => {
                reject(error);
              });
        }
    );
  }
}
