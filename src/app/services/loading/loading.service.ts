import {Injectable} from '@angular/core';
import {LoadingController} from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class LoadingService {

    private loading;

    constructor(private loadingController: LoadingController) {
    }

    async presentLoading(loadingMessage: string, loaderDuration: number) {

        this.loading = await this.loadingController.create({
            message: loadingMessage,
            duration: loaderDuration
        });
        await this.loading.present();

        const {role, data} = this.loading.onDidDismiss();
    }

    updateLoadingText(text: string) {
        const elem = document.querySelector('div.loading-wrapper div.loading-content');
        if (elem) {
            elem.innerHTML = text;
        }
    }

    dismissLoadingDefault() {
        this.loading.dismiss();
    }
}
