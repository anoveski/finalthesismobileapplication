import {Injectable} from '@angular/core';
import {AlertController} from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class AlertService {

    constructor(public alertController: AlertController) {
    }

    async presentAlert(title: string, subTitle: string, message: string, buttonTitle: string) {
        const alert = await this.alertController.create({
            header: title,
            subHeader: subTitle,
            message: message,
            buttons: [buttonTitle]
        });

        await alert.present();
    }

    async confirmAlert(title: string, subTitle: string, message: string, confirmButton: string, denyButton: string) {
        const alert = await this.alertController.create({
            header: title,
            subHeader: subTitle,
            message: message,
            buttons: [
                {
                    text: denyButton,
                    handler: () => {
                        alert.dismiss(false);
                        return false;
                    }
                },
                {
                    text: confirmButton,
                    handler: () => {
                        alert.dismiss(true);
                        return false;
                    }
                }]
        });

        await alert.present();
        return await alert.onDidDismiss()
            .then((data) => {
                return data;
            });
    }

    async presentAlertRadio(title: string, subTitle: string, message: string, inputs: any[], confirmButton: string, denyButton: string) {
        const alert = await this.alertController.create({
            header: title,
            subHeader: subTitle,
            message: message,
            inputs: inputs,
            buttons: [
                {
                    text: denyButton,
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        return {value: null};
                    }
                }, {
                    text: confirmButton,
                    role: 'confirm',
                    handler: (data) => {
                        return {value: data};
                    }
                }
            ]
        });

        await alert.present();
        return await alert.onDidDismiss()
            .then((data) => {
                return data;
            });
    }
}
