import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../../services/api/api.service';
import {Router} from '@angular/router';
import {Events} from '@ionic/angular';
import {AlertService} from '../../services/alert/alert.service';
import {LoadingService} from '../../services/loading/loading.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

    f: FormGroup;
    emailError = false;
    passwordError = false;

    constructor(private fb: FormBuilder,
                private router: Router,
                private events: Events,
                private loadingService: LoadingService,
                private alertService: AlertService,
                private apiService: ApiService) {
    }

    ngOnInit() {
        this.buildForm();
    }

    buildForm() {
        this.f = this.fb.group({
            username: new FormControl('', [
                Validators.required,
                Validators.email
            ]),
            password: new FormControl('', [
                Validators.required,
                Validators.minLength(8)
            ])
        });
    }

    signIn() {
        this.loadingService.presentLoading('Loading', null);
        this.apiService.login({
            email: this.f.controls.username.value,
            password: this.f.controls.password.value
        }).then((data: any) => {
            this.loadingService.dismissLoadingDefault();
            localStorage.setItem('userData', JSON.stringify(data.userData));
            localStorage.setItem('token', data.token);
            localStorage.setItem('userRoles', data.role);
            this.router.navigate(['browse-tickets']);
            this.events.publish('user-signed-in');
        }).catch((e) => {
            if (e.error.success === false) {
                this.loadingService.dismissLoadingDefault();
                this.alertService.presentAlert('Login failed!', '', 'Please enter valid credentials', 'Dismiss');
                this.emailError = e.error.errorLocation.emailError;
                this.passwordError = e.error.errorLocation.passwordError;
            }
        });
    }
}
