import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../services/api/api.service';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.page.html',
    styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

    profileData: any;
    typeOfUser: string;

    constructor(private apiService: ApiService) {
    }

    ngOnInit() {
        this.typeOfUser = localStorage.getItem('userRoles');

        this.apiService.getMyProfile()
            .then(data => {
                this.profileData = data;
            }).catch(e => {
            console.log(e);
        });
    }

}
