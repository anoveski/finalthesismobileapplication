import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ViewTicketDetailsPage } from './view-ticket-details.page';
import {NgxBarcodeModule} from 'ngx-barcode';

const routes: Routes = [
  {
    path: '',
    component: ViewTicketDetailsPage
  }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        NgxBarcodeModule
    ],
  declarations: [ViewTicketDetailsPage]
})
export class ViewTicketDetailsPageModule {}
