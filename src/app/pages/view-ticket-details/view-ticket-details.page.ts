import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BILLING_CURRENCY} from '../../../environments/environment';

@Component({
    selector: 'app-view-ticket-details',
    templateUrl: './view-ticket-details.page.html',
    styleUrls: ['./view-ticket-details.page.scss'],
})
export class ViewTicketDetailsPage implements OnInit {

    typeOfUser = localStorage.getItem('userRoles');
    ticketData: any;
    currency: string = BILLING_CURRENCY.currency;

    constructor(private route: ActivatedRoute,
                private router: Router) {
    }

    ngOnInit() {
        this.ticketData = JSON.parse(this.route.snapshot.queryParams.ticket);
    }

    purchaseTicket() {
        console.log('purchased');
        this.router.navigate(['purchase-ticket'], {
            queryParams: {
                ticket: JSON.stringify(this.ticketData)
            }
        });
    }
}
