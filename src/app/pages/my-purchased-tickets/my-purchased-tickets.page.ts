import {Component, OnInit} from '@angular/core';
import {RadioPopoverComponent} from '../../components/radio-popover/radio-popover.component';
import {PopoverController} from '@ionic/angular';
import {ApiService} from '../../services/api/api.service';
import {Router} from '@angular/router';
import {MyPurchasedTicketsPrivateUserEnum} from '../../enums/my-purchased-tickets-private-user-enum';
import {MyPurchasedTicketsBusinessUserEnum} from '../../enums/my-purchased-tickets-business-user-enum';

@Component({
    selector: 'app-my-purchased-tickets',
    templateUrl: './my-purchased-tickets.page.html',
    styleUrls: ['./my-purchased-tickets.page.scss'],
})
export class MyPurchasedTicketsPage implements OnInit {

    typeOfUser: string;
    selectedFilter: string;
    myPurchasedTicketsFilters: any[];

    myTickets: any[];
    MyPurchasedTicketsPrivateUserEnum: typeof MyPurchasedTicketsPrivateUserEnum = MyPurchasedTicketsPrivateUserEnum;
    MyPurchasedTicketsBusinessUserEnum: typeof MyPurchasedTicketsBusinessUserEnum = MyPurchasedTicketsBusinessUserEnum;

    constructor(public popoverController: PopoverController,
                private router: Router,
                private apiService: ApiService) {
    }

    ngOnInit() {
        this.typeOfUser = localStorage.getItem('userRoles');

        this.selectedFilter = 'active_purchased_all';

        if (this.typeOfUser === 'PRIVATE_USER') {

            this.myPurchasedTicketsFilters = [
                {value: 'active_purchased_all', viewValue: 'Purchased tickets', status: true},
                {value: 'inactive_tickets', viewValue: 'Purchased tickets history', status: false},
                {value: 'today_active_tickets', viewValue: 'Today`s tickets', status: false}
            ];
        } else if (this.typeOfUser === 'BUSINESS_USER') {
            this.myPurchasedTicketsFilters = [
                {value: 'active_purchased_all', viewValue: 'Active sold tickets', status: true},
                {value: 'today_active_tickets', viewValue: 'Today`s tickets', status: false},
                {value: 'inactive_tickets', viewValue: 'Sold tickets history', status: false},
            ];
        }

        this.getPurchasedTickets(this.selectedFilter);
    }

    async selectFilter(ev) {
        const popover = await this.popoverController.create({
            component: RadioPopoverComponent,
            componentProps: [this.myPurchasedTicketsFilters],
            event: ev,
            translucent: true
        });
        popover.onDidDismiss()
            .then((response: any) => {
                this.selectedFilter = response.data.value;
                this.myPurchasedTicketsFilters.forEach((filter, index) => {
                    if (filter.value === this.selectedFilter) {
                        this.myPurchasedTicketsFilters[index].status = true;
                        console.log(this.myPurchasedTicketsFilters);
                    } else {
                        this.myPurchasedTicketsFilters[index].status = false;
                    }
                });
                this.getPurchasedTickets(this.selectedFilter);
            })
            .catch(e => console.log(e));
        await popover.present();
    }

    getPurchasedTickets(filterValue: string) {
        this.myTickets = undefined;
        this.apiService.getPurchasedTickets({
            typeOfTickets: filterValue
        })
            .then((data: any) => {
                this.myTickets = data.tickets;
                console.log(this.myTickets);
            }).catch((error) => {
            console.log(error);
        });
    }

    ticketDetails(event) {
        this.router.navigate(['view-purchased-ticket-details'], {
            queryParams: {
                ticket: JSON.stringify(event)
            }
        });
    }
}
