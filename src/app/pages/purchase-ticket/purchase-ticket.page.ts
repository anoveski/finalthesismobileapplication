import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BILLING_CURRENCY} from '../../../environments/environment';
import {getDates} from '../../../helper-methods/get-dates-between/get-dates-between';
import {getFirstLastDayOfWeek} from '../../../helper-methods/get-first-last-day-of-week/get-first-last-day-of-week';
import {DatePipe} from '@angular/common';

@Component({
    selector: 'app-purchase-ticket',
    templateUrl: './purchase-ticket.page.html',
    styleUrls: ['./purchase-ticket.page.scss'],
})
export class PurchaseTicketPage implements OnInit {

    ticketDetails: any;
    selectedTicketType: string;
    currency: string = BILLING_CURRENCY.currency;

    weeklyRelation: any[];
    dailyRelation: any[];
    everyDayRelation: any;

    weeklyRelationDatesAndHours: any[];
    weeklyRelationAvailableDays: any[] = [];

    availableDepartureDates: any;

    availableStartDepartureHours: any;
    availableReturnDepartureHours: any;


    startLocationDate = '';
    startLocationHour = '';
    returnLocationDate = '';
    returnLocationHour = '';

    todayDate = new Date();
    now = new Date();
    endDate = new Date(this.now.setMonth(this.now.getMonth() + 2));
    weekDates: any[];
    datePickerObj: any;

    constructor(private route: ActivatedRoute,
                private router: Router,
                private datePipe: DatePipe) {
    }

    ngOnInit() {
        this.ticketDetails = JSON.parse(this.route.snapshot.queryParams.ticket);
        this.organizeTicketData();
        this.depFilter();
    }

    depFilter() {
        if (this.ticketDetails.typeOfRelation === 'weekly') {
            const firstAndLastDayOfWeekWeekly: any = getFirstLastDayOfWeek(this.todayDate);

            this.weekDates = getDates(new Date(firstAndLastDayOfWeekWeekly.firstDay), new Date(firstAndLastDayOfWeekWeekly.lastDay));
            if (this.ticketDetails.relationData.weeklyRelation.repeatEveryWeek === true) {
                this.datePickerObj = {
                    inputDate: this.todayDate,
                    fromDate: this.todayDate,
                    toDate: this.endDate,
                    mondayFirst: true,
                    dateFormat: 'YYYY-MM-DD',
                    disableWeekDays: this.weekDates.map((date: Date) => {
                        if (this.weeklyRelationAvailableDays.includes(this.datePipe.transform(date, 'EEE')) === false) {
                            return date.getDay();
                        } else {
                            return null;
                        }
                    }).filter(el => el !== null)
                };
            } else {
                const firstAndLastDayOfWeekWeeklyNoRepeat: any = getFirstLastDayOfWeek(Object.keys(this.availableDepartureDates)[0]);
                this.weekDates = getDates(new Date(firstAndLastDayOfWeekWeeklyNoRepeat.firstDay),
                    new Date(firstAndLastDayOfWeekWeeklyNoRepeat.lastDay));


                if (new Date(firstAndLastDayOfWeekWeeklyNoRepeat.firstDay).toISOString() > this.todayDate.toISOString()) {

                    this.datePickerObj = {
                        inputDate: new Date(firstAndLastDayOfWeekWeeklyNoRepeat.firstDay),
                        fromDate: new Date(firstAndLastDayOfWeekWeeklyNoRepeat.firstDay),
                        toDate: new Date(firstAndLastDayOfWeekWeeklyNoRepeat.lastDay),
                        mondayFirst: true,
                        dateFormat: 'YYYY-MM-DD',
                        disableWeekDays: this.weekDates.map((date: Date) => {
                            if (this.weeklyRelationAvailableDays.includes(this.datePipe.transform(date, 'EEE')) === false) {
                                return date.getDay();
                            } else {
                                return null;
                            }
                        }).filter(el => el !== null)
                    };
                } else {
                    this.datePickerObj = {
                        inputDate: this.todayDate,
                        fromDate: this.todayDate,
                        toDate: new Date(firstAndLastDayOfWeekWeeklyNoRepeat.lastDay),
                        mondayFirst: true,
                        dateFormat: 'YYYY-MM-DD',
                        disableWeekDays: this.weekDates.map((date: Date) => {
                            if (this.weeklyRelationAvailableDays.includes(this.datePipe.transform(date, 'EEE')) === false) {
                                return date.getDay();
                            } else {
                                return null;
                            }
                        }).filter(el => el !== null)
                    };
                }
            }

        } else if (this.ticketDetails.typeOfRelation === 'select_days') {
            const avlDepartureDatesArray: any[] = Object.keys(this.availableDepartureDates);

            const firstDate = new Date(avlDepartureDatesArray[0]);
            const lastDate = new Date(avlDepartureDatesArray[avlDepartureDatesArray.length - 1]);

            if (firstDate > this.todayDate) {

                console.log(avlDepartureDatesArray);

                this.weekDates = getDates(firstDate, lastDate);

                this.datePickerObj = {
                    inputDate: firstDate,
                    fromDate: firstDate,
                    toDate: lastDate,
                    mondayFirst: true,
                    dateFormat: 'YYYY-MM-DD',
                    disabledDates: this.weekDates.map((date: Date) => {
                        if (avlDepartureDatesArray.includes(date.toISOString()) === false) {
                            return date;
                        } else {
                            return null;
                        }
                    }).filter(el => el !== null)
                };
            } else {
                console.log(avlDepartureDatesArray);

                this.weekDates = getDates(this.todayDate, lastDate);

                this.datePickerObj = {
                    inputDate: this.todayDate,
                    fromDate: this.todayDate,
                    toDate: lastDate,
                    mondayFirst: true,
                    dateFormat: 'YYYY-MM-DD',
                    disabledDates: this.weekDates.map((date: Date) => {
                        if (avlDepartureDatesArray.includes(date.toISOString()) === false) {
                            return date;
                        } else {
                            return null;
                        }
                    }).filter(el => el !== null)
                };
            }

        } else if (this.ticketDetails.typeOfRelation === 'every_day') {
            this.datePickerObj = {
                inputDate: this.todayDate,
                fromDate: this.todayDate,
                toDate: this.endDate,
                mondayFirst: true,
                dateFormat: 'YYYY-MM-DD',
                disableWeekDays: []
            };
        }

    }

    selectTypeOfTicket(selectedTicket) {
        this.selectedTicketType = selectedTicket;
    }

    goToCheckout() {
        this.router.navigate(['step-enter-payment'], {
            queryParams: {
                userData: localStorage.getItem('userData'),
                ticketDetails: JSON.stringify(this.ticketDetails),
                ticketType: this.selectedTicketType,
                selectedDeparture: JSON.stringify({
                    startLocationDate: this.startLocationDate,
                    startLocationHour: this.startLocationHour,
                    returnLocationDate: this.returnLocationDate,
                    returnLocationHour: this.returnLocationHour
                })
            }
        });
    }

    selectedDate(ev, date) {
        const d = new Date(date);
        const selectedDate: string = this.datePipe.transform(new Date(d.setDate(d.getDate() - 1)).toISOString(), 'yyyy-MM-dd') + 'T22:00:00.000Z';

        if (this.ticketDetails.typeOfRelation === 'weekly') {
            this.weeklyRelationDatesAndHours.forEach(data => {

                data.availableDates.forEach(date => {
                    if (this.datePipe.transform(date, 'EEE') === this.datePipe.transform(selectedDate, 'EEE')) {
                        if (ev.target.id === 'startLocationDate') {
                            this.availableStartDepartureHours = data.availableHours.departureHoursStart;
                        } else if (ev.target.id === 'returnLocationDate') {
                            this.availableReturnDepartureHours = data.availableHours.departureHoursDestination;
                        }
                    }
                });

            });

        } else if (this.ticketDetails.typeOfRelation === 'select_days') {
            this.dailyRelation.forEach(data => {
                if (selectedDate === data.dayDate) {
                    if (ev.target.id === 'startLocationDate') {
                        this.availableStartDepartureHours = data.departureHoursStart;
                    } else if (ev.target.id === 'returnLocationDate') {
                        this.availableReturnDepartureHours = data.departureHoursDestination;
                    }
                }
            });
        } else if (this.ticketDetails.typeOfRelation === 'every_day') {
            if (ev.target.id === 'startLocationDate') {
                this.availableStartDepartureHours = this.everyDayRelation.departureHoursStart;
            } else if (ev.target.id === 'returnLocationDate') {
                this.availableReturnDepartureHours = this.everyDayRelation.departureHoursDestination;
            }
        }
    }

    organizeTicketData() {
        if (this.ticketDetails.typeOfRelation === 'weekly') {

            this.weeklyRelation = this.ticketDetails.relationData.weeklyRelation.selectedWeek;

            this.weeklyRelationDatesAndHours = this.weeklyRelation.map((data) => {
                let weeklyRelationDates;
                if (data.endDate === '') {
                    weeklyRelationDates = [new Date(data.startDate)];
                } else {
                    weeklyRelationDates = getDates(new Date(data.startDate), new Date(data.endDate));
                }

                return {
                    availableDates: weeklyRelationDates,
                    availableHours: {
                        departureHoursStart: data.departureHoursStart,
                        departureHoursDestination: data.departureHoursDestination
                    }
                };
            });

            const dates = this.weeklyRelationExtractDates(this.weeklyRelationDatesAndHours);

            this.availableDepartureDates = this.decodeMappedDates(dates.departureDate);


            Object.keys(this.availableDepartureDates).forEach((key) => {
                if (this.availableDepartureDates[key] === true) {
                    this.weeklyRelationAvailableDays.push(this.datePipe.transform(key, 'EEE'));
                }
            });


        } else if (this.ticketDetails.typeOfRelation === 'select_days') {

            this.dailyRelation = this.ticketDetails.relationData.dailyRelation;

            const filteredDailyRelation = this.dailyRelation.filter(relation => {
                if (relation.dayDate > (this.datePipe.transform(new Date(), 'yyyy-MM-dd') + 'T22:00:00.000Z')) {
                    return relation;
                }
            });

            const dates = this.selectDaysExtractDates(filteredDailyRelation);
            this.availableDepartureDates = this.decodeMappedDates(dates.departureDate);

        } else if (this.ticketDetails.typeOfRelation === 'every_day') {
            this.everyDayRelation = this.ticketDetails.relationData.everyDayRelation;

        }
    }

    weeklyRelationExtractDates(weeklyDaysArray) {
        const departureDateObj = new Map();

        weeklyDaysArray.forEach(day => {
            day.availableDates.forEach(d => {
                departureDateObj.set(d.toISOString(), true);
            });
        });

        return {departureDate: departureDateObj};
    }

    decodeMappedDates(departureDateObj) {
        return Array.from(departureDateObj).reduce((obj, [key, value]) => (
            Object.assign(obj, {[key]: value})
        ), {});
    }

    selectDaysExtractDates(selectDaysArray) {
        const departureDateObj = new Map();

        selectDaysArray.map((data) => {
            departureDateObj.set(data.dayDate, true);
        });

        return {departureDate: departureDateObj};
    }
}
