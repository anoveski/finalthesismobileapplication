import {Component, OnInit} from '@angular/core';
import {Platform, PopoverController} from '@ionic/angular';
import {TrackerPopoverComponent} from '../../components/tracker-popover/tracker-popover.component';
import {ApiService} from '../../services/api/api.service';
import * as socketIo from 'socket.io-client';
import {HOST_CREDENTIALS} from '../../../environments/environment';
import {PersonalVehicleSelectPopoverComponent} from '../../components/personal-vehicle-select-popover/personal-vehicle-select-popover.component';
import {LocationService} from '../../services/location/location.service';
import {Geolocation} from '@ionic-native/geolocation/ngx';

@Component({
    selector: 'app-tracker-map',
    templateUrl: './tracker-map.page.html',
    styleUrls: ['./tracker-map.page.scss'],
})
export class TrackerMapPage implements OnInit {

    typeOfUser = localStorage.getItem('userRoles');

    availableDestinations: any[];
    pinTitle: string;
    socket = socketIo(HOST_CREDENTIALS.hostURL);
    latitude: number;
    longitude: number;
    showMarker = false;
    mapZoom = 8;

    mapLatitude = 41.785930;
    mapLongitude = 21.692989;

    myVehicles: any = [
        {
            name: 'Vehicle 1'
        },
        {
            name: 'Vehicle 2'
        },
        {
            name: 'Vehicle 3'
        },
        {
            name: 'Vehicle 4'
        }
    ];

    constructor(public popoverController: PopoverController,
                public platform: Platform,
                private geolocation: Geolocation,
                private locationService: LocationService,
                private apiService: ApiService) {
    }

    ngOnInit() {
        this.apiService.getAvailableDestinations()
            .then((data: any[]) => {
                this.availableDestinations = data;
            }).catch((e) => {
            console.log(e);
        });

        if (this.platform.is('android') === true || this.platform.is('ios') === true) {
            this.deviceSetCurrentPosition();
        } else {
            this.browserSetCurrentPosition();
        }
    }

    async trackPersonalVehicles(ev) {
        const popover = await this.popoverController.create({
            component: PersonalVehicleSelectPopoverComponent,
            componentProps: [this.myVehicles],
            event: ev,
            translucent: true
        });
        popover.onDidDismiss()
            .then((response: any) => {
                if (response.data.selectedVehicle !== undefined) {
                    this.showActiveVehicles(response.data);
                }
            })
            .catch(e => console.log(e));
        await popover.present();
    }

    async trackPublicVehicles(ev) {
        const popover = await this.popoverController.create({
            component: TrackerPopoverComponent,
            componentProps: [this.availableDestinations],
            event: ev,
            translucent: true
        });
        popover.onDidDismiss()
            .then((response: any) => {
                if (response.data !== undefined) {
                    this.showActiveVehicles(response.data);
                }
            })
            .catch(e => console.log(e));
        await popover.present();
    }

    showActiveVehicles(vehicleData) {
        if (vehicleData.startLocation !== '' && vehicleData.startLocation !== undefined && vehicleData.destinationLocation !== ''
            && vehicleData.destinationLocation !== undefined) {

            this.pinTitle = vehicleData.startLocation.townOrMunicipality + ', ' + vehicleData.startLocation.country.name + ' --> ' +
                vehicleData.destinationLocation.townOrMunicipality + ', ' + vehicleData.destinationLocation.country.name;
            this.trackVehicleEvent(true);

        } else if (vehicleData.selectedVehicle !== '' && vehicleData.selectedVehicle !== undefined) {
            this.pinTitle = vehicleData.selectedVehicle.name;
            this.trackVehicleEvent(true);
        }
    }

    trackVehicleEvent(startTracking: boolean) {
        this.socket.on('vehicleLocation', data => {
            console.log(data);
            this.showMarker = true;
            this.mapZoom = 18;
            this.latitude = data.lat;
            this.longitude = data.lon;

            if (startTracking === true) {
                this.mapLatitude = data.lat;
                this.mapLongitude = data.lon;
                startTracking = false;
            }
        });
    }

    deviceSetCurrentPosition() {
        this.geolocation.getCurrentPosition()
            .then((position) => {
                this.mapLatitude = position.coords.latitude;
                this.mapLongitude = position.coords.longitude;
                this.mapZoom = 12;
            }).catch((error) => {
            console.log('Error getting location', error);
            this.deviceSetCurrentPosition();
        });
    }

    browserSetCurrentPosition() {
        this.locationService.getCurrentPosition()
            .then((position: any) => {
                this.mapLatitude = position.latitude;
                this.mapLongitude = position.longitude;
                this.mapZoom = 12;
            }).catch((error) => {
            console.log(error);
        });
    }
}
