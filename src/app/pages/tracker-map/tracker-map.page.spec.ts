import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrackerMapPage } from './tracker-map.page';

describe('TrackerMapPage', () => {
  let component: TrackerMapPage;
  let fixture: ComponentFixture<TrackerMapPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrackerMapPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrackerMapPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
