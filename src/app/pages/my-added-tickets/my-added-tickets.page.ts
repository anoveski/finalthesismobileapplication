import {Component, OnInit} from '@angular/core';
import {RadioPopoverComponent} from '../../components/radio-popover/radio-popover.component';
import {PopoverController} from '@ionic/angular';
import {ApiService} from '../../services/api/api.service';
import {MyAddedTicketsEnum} from '../../enums/my-added-tickets-enum';
import {Router} from '@angular/router';

@Component({
    selector: 'app-my-added-tickets',
    templateUrl: './my-added-tickets.page.html',
    styleUrls: ['./my-added-tickets.page.scss'],
})
export class MyAddedTicketsPage implements OnInit {

    selectedFilter = 'active';
    myAddedTicketsFilters: any[] = [
        {value: 'active', viewValue: 'Active Tickets'},
        {value: 'inactive', viewValue: 'Inactive Tickets'},
        {value: 'all', viewValue: 'All'},
        {value: 'weekly', viewValue: 'Weekly'},
        {value: 'select_days', viewValue: 'Select Days'},
        {value: 'every_day', viewValue: 'Every Day'}
    ];

    myTickets: any[];
    MyAddedTicketsEnum: typeof MyAddedTicketsEnum = MyAddedTicketsEnum;

    constructor(public popoverController: PopoverController,
                private router: Router,
                private apiService: ApiService) {
    }

    ngOnInit() {
        this.getTickets(this.selectedFilter);
    }

    async selectFilter(ev) {
        const popover = await this.popoverController.create({
            component: RadioPopoverComponent,
            componentProps: [this.myAddedTicketsFilters],
            event: ev,
            translucent: true
        });
        popover.onDidDismiss()
            .then((response: any) => {
                console.log(response);
                this.selectedFilter = response.data.value;
                this.myAddedTicketsFilters.forEach((filter, index) => {
                    if (filter.value === this.selectedFilter) {
                        this.myAddedTicketsFilters[index].status = true;
                    } else {
                        this.myAddedTicketsFilters[index].status = false;
                    }
                });

                this.getTickets(this.selectedFilter);
            })
            .catch(e => console.log(e));
        await popover.present();
    }

    getTickets(chosenFilter) {
        this.myTickets = undefined;
        this.apiService.getCompanyTickets({
            ticketType: chosenFilter
        })
            .then((data: any[]) => {
                console.log(data);
                this.myTickets = data;
            })
            .catch(e => {
                console.log(e);
            });
    }

    viewTicketDetails(event) {
        console.log(event);
        this.router.navigate(['view-ticket-details'], {
            queryParams: {
                ticket: JSON.stringify(event)
            }
        });
    }
}
