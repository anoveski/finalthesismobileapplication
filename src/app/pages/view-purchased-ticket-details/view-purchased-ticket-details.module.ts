import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ViewPurchasedTicketDetailsPage } from './view-purchased-ticket-details.page';
import {NgxBarcodeModule} from 'ngx-barcode';

const routes: Routes = [
  {
    path: '',
    component: ViewPurchasedTicketDetailsPage
  }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        NgxBarcodeModule
    ],
  declarations: [ViewPurchasedTicketDetailsPage]
})
export class ViewPurchasedTicketDetailsPageModule {}
