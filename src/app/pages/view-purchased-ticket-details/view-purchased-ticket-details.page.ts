import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BILLING_CURRENCY} from '../../../environments/environment';
import {Platform} from '@ionic/angular';

@Component({
    selector: 'app-view-purchased-ticket-details',
    templateUrl: './view-purchased-ticket-details.page.html',
    styleUrls: ['./view-purchased-ticket-details.page.scss'],
})
export class ViewPurchasedTicketDetailsPage implements OnInit {

    goBack: string;

    ticketData: any;
    currency: string = BILLING_CURRENCY.currency;

    constructor(private route: ActivatedRoute,
                private platform: Platform) {
    }

    ngOnInit() {
        this.ticketData = JSON.parse(this.route.snapshot.queryParams.ticket);
        this.goBack = this.route.snapshot.queryParams.goBack;
        console.log(this.goBack);
        console.log(this.ticketData);
    }


    // ionViewDidEnter() {
    //     if (this.goBack === 'false') {
    //         this.platform.backButton.subscribeWithPriority(9999, () => {
    //             // Do nothing
    //         });
    //     }
    // }
    //
    // ionViewDidLeave() {
    //     if (this.goBack === 'false') {
    //         this.platform.backButton.unsubscribe();
    //     }
    // }
}
