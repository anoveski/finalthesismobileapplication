import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../services/api/api.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-browse-tickets',
    templateUrl: './browse-tickets.page.html',
    styleUrls: ['./browse-tickets.page.scss'],
})
export class BrowseTicketsPage implements OnInit {

    availableDestinations: any;
    availableTickets: any[];
    loadingSpinner = false;

    constructor(private apiService: ApiService,
                private router: Router) {
    }

    ngOnInit() {
        this.apiService.getAvailableDestinations()
            .then((data) => {
                this.availableDestinations = data;
            }).catch((e) => {
            console.log(e);
        });
    }

    browseTickets(event) {
        this.loadingSpinner = true;
        this.apiService.browseDestinations(event)
            .then((data: any[]) => {
                this.loadingSpinner = false;
                this.availableTickets = data;
            }).catch((error) => {
            this.loadingSpinner = false;
            console.log(error);
        });
    }

    viewTicketDetails(event) {
        this.router.navigate(['view-ticket-details'], {
            queryParams: {
                ticket: JSON.stringify(event)
            }
        });
    }
}
