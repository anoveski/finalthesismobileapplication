import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../services/api/api.service';
import {AlertService} from '../../services/alert/alert.service';
import {LoadingService} from '../../services/loading/loading.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-register-account',
    templateUrl: './register-account.page.html',
    styleUrls: ['./register-account.page.scss'],
})
export class RegisterAccountPage implements OnInit {

    disableRegister = false;
    registerSuccess = false;

    constructor(private apiService: ApiService,
                private router: Router,
                private loadingService: LoadingService,
                private alertService: AlertService) {
    }

    ngOnInit() {
    }

    signUp(credentials) {
        this.disableRegister = true;
        this.loadingService.presentLoading('Signing up', null);
        this.apiService.registerAccount(credentials)
            .then(data => {
                this.disableRegister = false;
                this.registerSuccess = true;
                this.loadingService.dismissLoadingDefault();
                this.alertService.presentAlert('Success!', '', 'Account successfully registered',
                    'Dismiss');
                this.router.navigate(['/login']);
                console.log(data);
            })
            .catch((e) => {
                this.disableRegister = false;
                this.loadingService.dismissLoadingDefault();
                this.alertService.presentAlert('Failed!', '', e.error.message, 'Dismiss');
            });
    }
}
