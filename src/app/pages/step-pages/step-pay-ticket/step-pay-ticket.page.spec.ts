import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepPayTicketPage } from './step-pay-ticket.page';

describe('StepPayTicketPage', () => {
  let component: StepPayTicketPage;
  let fixture: ComponentFixture<StepPayTicketPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepPayTicketPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepPayTicketPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
