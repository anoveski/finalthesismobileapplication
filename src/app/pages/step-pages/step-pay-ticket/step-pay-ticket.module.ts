import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { StepPayTicketPage } from './step-pay-ticket.page';

const routes: Routes = [
  {
    path: '',
    component: StepPayTicketPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [StepPayTicketPage]
})
export class StepPayTicketPageModule {}
