import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BILLING_CURRENCY} from '../../../../environments/environment';
import {AlertService} from '../../../services/alert/alert.service';
import {ApiService} from '../../../services/api/api.service';
import {LoadingService} from '../../../services/loading/loading.service';

@Component({
    selector: 'app-step-pay-ticket',
    templateUrl: './step-pay-ticket.page.html',
    styleUrls: ['./step-pay-ticket.page.scss'],
})
export class StepPayTicketPage implements OnInit {

    currency: string = BILLING_CURRENCY.currency;

    passengerData: any;
    ticketDetails: any;
    ticketType: string;
    selectedDeparture: any;
    totalPrice: string;

    enterPaymentData: string;

    constructor(private route: ActivatedRoute,
                private apiService: ApiService,
                private router: Router,
                private loadingService: LoadingService,
                private alertService: AlertService) {
        this.passengerData = JSON.parse(JSON.parse(this.route.snapshot.queryParams.ticketFullData).userData);
        this.ticketDetails = JSON.parse(JSON.parse(this.route.snapshot.queryParams.ticketFullData).ticketDetails);
        this.ticketType = JSON.parse(this.route.snapshot.queryParams.ticketFullData).ticketType;
        this.selectedDeparture = JSON.parse(JSON.parse(this.route.snapshot.queryParams.ticketFullData).selectedDeparture);
        this.enterPaymentData = this.route.snapshot.queryParams.paymentToken;
    }

    ngOnInit() {
        if (this.ticketType === 'oneWayTicket') {
            this.totalPrice = this.ticketDetails.oneWayPrice;
        } else if (this.ticketType === 'returnTicket') {
            this.totalPrice = this.ticketDetails.returnTicketPrice;
        }
    }

    confirmAndPayTicket() {
        this.alertService.confirmAlert('Buy ticket', '', 'Are you sure you want to purchase this ticket?', 'Yes', 'No')
            .then((response: any) => {
                if (response.data === true) {
                    console.log(response);
                    console.log(this.enterPaymentData);
                    this.loadingService.presentLoading('Purchasing', null);
                    this.apiService.purchaseTicket({
                        stripeUserPurchaseTicket: this.enterPaymentData,
                        ticketId: this.ticketDetails._id,
                        ticketType: this.ticketType,
                        passenger: this.passengerData,
                        selectedDeparture: this.selectedDeparture
                    }).then((data: any) => {
                        console.log(data);
                        if (data.success === true) {
                            this.loadingService.dismissLoadingDefault();
                            this.alertService.presentAlert('Successfully purchased', 'You have successfully purchased a ticket.',
                                'Your ticket has been sent to your email address.', 'Ok');
                            this.router.navigate(['view-purchased-ticket-details'], {
                                queryParams: {
                                    ticket: JSON.stringify(data.purchasedTicketData),
                                    goBack: 'false'
                                }
                            });
                        }

                    }).catch((error) => {
                        this.loadingService.dismissLoadingDefault();
                        console.log(error);
                    });
                }
            }).catch(e => console.log(e));
    }
}
