import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { StepEnterPaymentPage } from './step-enter-payment.page';

const routes: Routes = [
  {
    path: '',
    component: StepEnterPaymentPage
  }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule
    ],
  declarations: [StepEnterPaymentPage]
})
export class StepEnterPaymentPageModule {}
