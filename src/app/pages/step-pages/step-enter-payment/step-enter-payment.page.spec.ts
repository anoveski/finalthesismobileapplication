import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepEnterPaymentPage } from './step-enter-payment.page';

describe('StepEnterPaymentPage', () => {
  let component: StepEnterPaymentPage;
  let fixture: ComponentFixture<StepEnterPaymentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepEnterPaymentPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepEnterPaymentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
