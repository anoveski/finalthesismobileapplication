import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Stripe} from '@ionic-native/stripe/ngx';
import {STRIPE_PUBLIC_KEY} from '../../../../environments/environment';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {LoadingService} from '../../../services/loading/loading.service';
import {NavController} from '@ionic/angular';

@Component({
    selector: 'app-step-enter-payment',
    templateUrl: './step-enter-payment.page.html',
    styleUrls: ['./step-enter-payment.page.scss'],
})
export class StepEnterPaymentPage implements OnInit {

    f: FormGroup;

    constructor(private route: ActivatedRoute,
                private loadingService: LoadingService,
                private router: Router,
                private fb: FormBuilder,
                private navCtrl: NavController,
                private stripe: Stripe) {

        this.stripe.setPublishableKey(STRIPE_PUBLIC_KEY.stripePublicKey);
    }

    ngOnInit() {
        this.buildForm();
    }

    buildForm() {
        this.f = this.fb.group({
            cardNumber: new FormControl('', [
                Validators.required,
                Validators.minLength(16)
            ]),
            expireMonth: new FormControl('', [
                Validators.required,
                Validators.minLength(2)
            ]),
            expireYear: new FormControl('', [
                Validators.required,
                Validators.minLength(2)
            ]),
            cvc: new FormControl('', [
                Validators.required,
                Validators.minLength(3)
            ])
        });
    }


    processPayment() {
        this.loadingService.presentLoading('Processing', null);
        this.stripe.createCardToken({
            number: this.f.controls.cardNumber.value,
            expMonth: this.f.controls.expireMonth.value,
            expYear: this.f.controls.expireYear.value,
            cvc: this.f.controls.cvc.value,
        })
            .then((token: any) => {
                console.log(token);
                console.log(token.id);
                this.loadingService.dismissLoadingDefault();
                this.goToNextStep(token.id);
            })
            .catch((error) => {
                console.error(error);
                this.loadingService.dismissLoadingDefault();
            });
    }

    goToNextStep(token) {
        this.router.navigate(['step-pay-ticket'], {
            queryParams: {
                ticketFullData: JSON.stringify(this.route.snapshot.queryParams),
                paymentToken: token
            }
        });
    }

    goBack() {
        this.navCtrl.back();
    }
}
