export enum MyPurchasedTicketsBusinessUserEnum {
    active_purchased_all = 'Active purchased Tickets',
    today_active_tickets = 'Today`s tickets',
    inactive_tickets = 'History purchased tickets'
}
