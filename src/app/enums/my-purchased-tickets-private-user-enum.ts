export enum MyPurchasedTicketsPrivateUserEnum {
    active_purchased_all = 'Purchased tickets',
    inactive_tickets = 'History purchased tickets',
    today_active_tickets = 'Today`s tickets'
}
