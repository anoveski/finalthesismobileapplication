export enum MyAddedTicketsEnum {
    active = 'Active Tickets',
    inactive = 'Inactive Tickets',
    all = 'All',
    weekly = 'Weekly',
    select_days = 'Select Days',
    every_day = 'Every Day'
}
