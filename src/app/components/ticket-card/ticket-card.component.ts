import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {BILLING_CURRENCY} from '../../../environments/environment';

@Component({
    selector: 'app-ticket-card',
    templateUrl: './ticket-card.component.html',
    styleUrls: ['./ticket-card.component.scss'],
})
export class TicketCardComponent implements OnInit {

    @Output() ticketClickEvent: EventEmitter<object> = new EventEmitter<object>();

    @Input() ticketData: any;
    currency: string = BILLING_CURRENCY.currency;

    constructor() {
    }

    ngOnInit() {
    }

    openTicket(ticket) {
        this.ticketClickEvent.emit(ticket);
    }
}
