import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'app-private-user-profile-card',
    templateUrl: './private-user-profile-card.component.html',
    styleUrls: ['./private-user-profile-card.component.scss'],
})
export class PrivateUserProfileCardComponent implements OnInit {

    @Input() privateUserProfileData: any;

    constructor() {
    }

    ngOnInit() {
    }

}
