import {Component, OnInit} from '@angular/core';
import {NavParams, PopoverController} from '@ionic/angular';

@Component({
    selector: 'app-personal-vehicle-select-popover',
    templateUrl: './personal-vehicle-select-popover.component.html',
    styleUrls: ['./personal-vehicle-select-popover.component.scss'],
})
export class PersonalVehicleSelectPopoverComponent implements OnInit {

    myVehicles: any;
    selectedVehicle: any;

    constructor(public navParams: NavParams,
                private popoverCtrl: PopoverController) {
        this.myVehicles = this.navParams.data[0];
    }

    ngOnInit() {
    }

    async personalVehicle(event) {
        await this.popoverCtrl.dismiss({selectedVehicle: event.name});
    }
}
