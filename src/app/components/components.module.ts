import {NgModule} from '@angular/core';
import {IonicModule} from '@ionic/angular';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {PurchasedTicketCardComponent} from './purchased-ticket-card/purchased-ticket-card.component';
import {PrivateUserProfileCardComponent} from './private-user-profile-card/private-user-profile-card.component';
import {BusinessProfileCardComponent} from './business-profile-card/business-profile-card.component';
import {PipesModule} from '../pipes/pipes.module';
import {BrowseDestinationCardComponent} from './browse-destination-card/browse-destination-card.component';
import {TicketCardComponent} from './ticket-card/ticket-card.component';
import {RegisterPrivateUserComponent} from './register-private-user/register-private-user.component';
import {SpinnerComponent} from './spinner/spinner.component';

@NgModule({
    declarations: [
        PurchasedTicketCardComponent,
        PrivateUserProfileCardComponent,
        BusinessProfileCardComponent,
        BrowseDestinationCardComponent,
        TicketCardComponent,
        RegisterPrivateUserComponent,
        SpinnerComponent
    ],
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        PipesModule,
        ReactiveFormsModule
    ],
    exports: [
        PurchasedTicketCardComponent,
        PrivateUserProfileCardComponent,
        BusinessProfileCardComponent,
        BrowseDestinationCardComponent,
        TicketCardComponent,
        RegisterPrivateUserComponent,
        SpinnerComponent
    ]
})
export class ComponentsModule {
}
