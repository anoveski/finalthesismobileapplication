import {Component, OnInit} from '@angular/core';
import {NavParams, PopoverController} from '@ionic/angular';

@Component({
    selector: 'app-tracker-popover',
    templateUrl: './tracker-popover.component.html',
    styleUrls: ['./tracker-popover.component.scss'],
})
export class TrackerPopoverComponent implements OnInit {

    popoverData: any;

    constructor(public navParams: NavParams,
                private popoverCtrl: PopoverController) {
        this.popoverData = this.navParams.data[0];
    }

    ngOnInit() {
    }

    async trackDestinationVehicle(event) {
        await this.popoverCtrl.dismiss(event);
    }
}
