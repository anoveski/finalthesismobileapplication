import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {BILLING_CURRENCY} from '../../../environments/environment';

@Component({
    selector: 'app-purchased-ticket-card',
    templateUrl: './purchased-ticket-card.component.html',
    styleUrls: ['./purchased-ticket-card.component.scss'],
})
export class PurchasedTicketCardComponent implements OnInit {

    @Output() ticketClickEvent: EventEmitter<object> = new EventEmitter<object>();

    @Input() ticket: any;
    currency: string = BILLING_CURRENCY.currency;

    constructor() {
    }

    ngOnInit() {
    }

    cardClick(ticket: any) {
        this.ticketClickEvent.emit(ticket);
    }
}
