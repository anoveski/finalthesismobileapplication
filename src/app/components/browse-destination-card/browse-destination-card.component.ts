import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
    selector: 'app-browse-destination-card',
    templateUrl: './browse-destination-card.component.html',
    styleUrls: ['./browse-destination-card.component.scss'],
})
export class BrowseDestinationCardComponent implements OnInit {

    @Output() browseTickets: EventEmitter<object> = new EventEmitter<object>();

    @Input() destinations: any;
    public startLocation: any;
    public destinationLocation: any;

    constructor() {
    }

    ngOnInit() {
    }

    searchDestinations() {
        if (this.startLocation !== undefined && this.destinationLocation !== undefined) {
            this.browseTickets.emit({
                startLocation: this.startLocation,
                destinationLocation: this.destinationLocation
            });
        }
    }
}
