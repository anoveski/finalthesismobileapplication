import {Component, OnInit} from '@angular/core';
import {NavParams, PopoverController} from '@ionic/angular';

@Component({
    selector: 'app-radio-popover',
    templateUrl: './radio-popover.component.html',
    styleUrls: ['./radio-popover.component.scss'],
})
export class RadioPopoverComponent implements OnInit {

    popoverItem: any;

    constructor(public navParams: NavParams,
                private popoverCtrl: PopoverController) {
    }

    ngOnInit() {
        this.popoverItem = this.navParams.data[0];
    }

    async radioChecked(item) {
        await this.popoverCtrl.dismiss(item, this.popoverItem.filterChoice);
    }
}
