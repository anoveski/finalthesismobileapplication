import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RadioPopoverPage } from './radio-popover.page';

describe('RadioPopoverPage', () => {
  let component: RadioPopoverPage;
  let fixture: ComponentFixture<RadioPopoverPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RadioPopoverPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RadioPopoverPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
