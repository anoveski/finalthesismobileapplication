import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'browse-tickets',
    pathMatch: 'full'
  },
  {
    path: 'browse-tickets',
    loadChildren: () => import('./pages/browse-tickets/browse-tickets.module').then(m => m.BrowseTicketsPageModule)
  },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'my-purchased-tickets', loadChildren: './pages/my-purchased-tickets/my-purchased-tickets.module#MyPurchasedTicketsPageModule' },
  { path: 'view-ticket-details', loadChildren: './pages/view-ticket-details/view-ticket-details.module#ViewTicketDetailsPageModule' },
  { path: 'profile', loadChildren: './pages/profile/profile.module#ProfilePageModule' },
  { path: 'tracker-map', loadChildren: './pages/tracker-map/tracker-map.module#TrackerMapPageModule' },
  { path: 'browse-tickets', loadChildren: './pages/browse-tickets/browse-tickets.module#BrowseTicketsPageModule' },
  { path: 'view-purchased-ticket-details', loadChildren: './pages/view-purchased-ticket-details/view-purchased-ticket-details.module#ViewPurchasedTicketDetailsPageModule' },
  { path: 'my-added-tickets', loadChildren: './pages/my-added-tickets/my-added-tickets.module#MyAddedTicketsPageModule' },
  { path: 'purchase-ticket', loadChildren: './pages/purchase-ticket/purchase-ticket.module#PurchaseTicketPageModule' },
  { path: 'register-account', loadChildren: './pages/register-account/register-account.module#RegisterAccountPageModule' },
  { path: 'step-enter-payment', loadChildren: './pages/step-pages/step-enter-payment/step-enter-payment.module#StepEnterPaymentPageModule' },
  { path: 'step-pay-ticket', loadChildren: './pages/step-pages/step-pay-ticket/step-pay-ticket.module#StepPayTicketPageModule' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
