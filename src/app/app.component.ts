import {Component} from '@angular/core';

import {Events, Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import { HeaderColor } from '@ionic-native/header-color/ngx';
import {AuthService} from './services/auth/auth.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss']
})
export class AppComponent {

    public appPages = [
        {
            title: 'Browse Tickets',
            url: '/browse-tickets',
            direction: 'root',
            icon: 'bookmarks',
            visible: true
        },
        {
            title: 'Tracker',
            url: '/tracker-map',
            direction: 'forward',
            icon: 'map',
            visible: true
        }
    ];
    typeOfUser: string;

    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private headerColor: HeaderColor,
        public router: Router,
        private events: Events,
        public authService: AuthService
    ) {
        this.initializeApp();

    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.headerColor.tint('#f8f8f8');
            this.statusBar.overlaysWebView(false);
            this.statusBar.backgroundColorByHexString('#f8f8f8');
            this.splashScreen.hide();
            this.typeOfUser = localStorage.getItem('userRoles');

            this.events.subscribe('user-signed-in', () => {
                this.typeOfUser = localStorage.getItem('userRoles');
            });
        });
    }

    public logOut(): void {
        localStorage.removeItem('userData');
        localStorage.removeItem('token');
        localStorage.removeItem('userRoles');
        this.router.navigate(['/browse-tickets']);
    }
}
