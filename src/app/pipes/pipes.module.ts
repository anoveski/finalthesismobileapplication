import {NgModule} from '@angular/core';
import {DecodePhoneNumberPipe} from './decode-phone-number/decode-phone-number.pipe';

@NgModule({
    declarations: [
        DecodePhoneNumberPipe
    ],
    providers: [
        DecodePhoneNumberPipe
    ],
    exports: [
        DecodePhoneNumberPipe
    ]
})
export class PipesModule {
}
