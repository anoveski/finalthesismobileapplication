import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';

import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {HttpClientModule} from '@angular/common/http';
import {RadioPopoverComponent} from './components/radio-popover/radio-popover.component';
import {NgxBarcodeModule} from 'ngx-barcode';
import {TrackerPopoverComponent} from './components/tracker-popover/tracker-popover.component';
import {ComponentsModule} from './components/components.module';
import {PersonalVehicleSelectPopoverComponent} from './components/personal-vehicle-select-popover/personal-vehicle-select-popover.component';
import {FormsModule} from '@angular/forms';
import {HeaderColor} from '@ionic-native/header-color/ngx';
import {GOOGLE_API_KET} from '../environments/environment';
import {AgmCoreModule} from '@agm/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import {DatePipe} from '@angular/common';
import {Ionic4DatepickerModule} from '@logisticinfotech/ionic4-datepicker';
import {Stripe} from '@ionic-native/stripe/ngx';

@NgModule({
    declarations: [
        AppComponent,
        RadioPopoverComponent,
        TrackerPopoverComponent,
        PersonalVehicleSelectPopoverComponent
    ],
    entryComponents: [
        RadioPopoverComponent,
        TrackerPopoverComponent,
        PersonalVehicleSelectPopoverComponent
    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot(),
        AgmCoreModule.forRoot({
            apiKey: GOOGLE_API_KET.apiKey,
            libraries: ['places']
        }),
        AppRoutingModule,
        HttpClientModule,
        NgxBarcodeModule,
        ComponentsModule,
        FormsModule,
        Ionic4DatepickerModule
    ],
    providers: [
        StatusBar,
        SplashScreen,
        Geolocation,
        DatePipe,
        HeaderColor,
        Stripe,
        {provide: RouteReuseStrategy, useClass: IonicRouteStrategy}
    ],
    exports: [
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
